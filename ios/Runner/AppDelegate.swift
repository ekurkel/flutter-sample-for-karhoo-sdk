import UIKit
import Flutter
import KarhooUISDK
import Braintree

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController

    let flutterChannel = FlutterMethodChannel(name: "Karhoo/channel", binaryMessenger: controller.binaryMessenger)
       
    flutterChannel.setMethodCallHandler({
          (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
           if call.method == "navigateToKarhooScreen" {
               KarhooUI.set(configuration: KarhooConfig())
               BTAppSwitch.setReturnURLScheme("ltd.ember.lochlomond")
               
              let bookingScreen = KarhooUI().screens().booking().buildBookingScreen() as? BookingScreen
              controller.present(bookingScreen!, animated: true)
               return
           } else {
               result(FlutterMethodNotImplemented)
               return
           }
    })
    
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
