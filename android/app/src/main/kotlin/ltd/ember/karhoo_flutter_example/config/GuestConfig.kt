package ltd.ember.aberdeen

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.karhoo.sdk.analytics.AnalyticProvider
import com.karhoo.sdk.api.KarhooEnvironment
import com.karhoo.sdk.api.model.AuthenticationMethod
import com.karhoo.uisdk.KarhooUISDKConfiguration
import ltd.ember.karhoo_flutter_example.R

class GuestConfig(private val context: Context) : KarhooUISDKConfiguration {

    override fun context(): Context {
        return context
    }

    override fun environment(): KarhooEnvironment {
        return KarhooEnvironment.Sandbox()
    }

    override fun analyticsProvider(): AnalyticProvider? {
        return null
    }

    override fun authenticationMethod(): AuthenticationMethod {
        return AuthenticationMethod.Guest(
            identifier = "FQnITy6NWgtj00HdEC5EflKHvqa0kSVq",
            referer = "https://mobile.karhoo.com",
            organisationId = "7f561990-8141-4192-bbf4-538fddaa3179"
        )
    }

    override fun logo(): Drawable? {
        return ContextCompat.getDrawable(context, R.mipmap.ic_launcher)
    }
}