package ltd.ember.karhoo_flutter_example

import android.app.Activity
import android.content.Intent
import com.karhoo.sdk.api.model.Position
import com.karhoo.sdk.api.model.TripInfo
import com.karhoo.sdk.api.model.TripLocationInfo
import com.karhoo.sdk.api.network.request.PassengerDetails
import com.karhoo.uisdk.KarhooUISDK
import com.karhoo.uisdk.base.booking.BookingCodes
import com.karhoo.uisdk.screen.booking.BookingActivity
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import ltd.ember.aberdeen.GuestConfig
import java.text.DateFormat
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*

class MainActivity: FlutterActivity() {
    private val CHANNEL = "Karhoo/channel"
    private lateinit var result : MethodChannel.Result

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == "navigateToKarhooScreen") {
                this.result = result
                applyGuestConfig()
                goToBooking(call)
            } else {
                result.notImplemented()
            }
        }
    }

    private fun applyGuestConfig() {
        KarhooUISDK.apply {
            setConfiguration(
                    GuestConfig(
                            applicationContext
                    )
            )
        }
    }

    private fun goToBooking(call: MethodCall) {
        val originAddress: String? = call.argument("origin_address")
        val originLatitude : Double? = call.argument("origin_lat")
        val originLongitude : Double? = call.argument("origin_lng")
        val destinationAddress: String? = call.argument("destination_address")
        val destinationLatitude : Double? = call.argument("destination_lat")
        val destinationLongitude : Double? = call.argument("destination_lng")
        val firstName : String? = call.argument("first_name")
        val lastName : String? = call.argument("last_name")
        val email : String? = call.argument("email")
        val phone : String? = call.argument("phone")

        val formatter: DateFormat = SimpleDateFormat("dd/MM/yy, HH:mm", Locale.ENGLISH)
        val date: Date = formatter.parse(call.argument("date")!!)!!

        val myReqCode = 101
        val intent = BookingActivity.Builder.builder
                .tripDetails(
                    TripInfo(
                        dateScheduled = date,
                        origin = TripLocationInfo(originAddress!!, Position(originLatitude!!, originLongitude!!)),
                        destination = TripLocationInfo(destinationAddress!!, Position(destinationLatitude!!, destinationLongitude!!))
                )
                )
                .passengerDetails(PassengerDetails(firstName, lastName, email, phone))
                .buildForOnActivityResultCallback(this)
        startActivityForResult(intent, myReqCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == 101) {
            val bookedTrip = data?.getParcelableExtra<TripInfo>(BookingCodes.BOOKED_TRIP)
            this.result.success(bookedTrip)
        } else {
            this.result.success(false)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
