import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: isLoading
              ? CircularProgressIndicator()
              : ElevatedButton(
                  onPressed: () async {
                    setLoading(true);
                    await MethodChannel('Karhoo/channel')
                        .invokeMethod('navigateToKarhooScreen', {
                      'origin_address': 'Dundee, UK',
                      'origin_lat': 56.4620,
                      'origin_lng': -2.9707,
                      'destination_address': 'Aberdeen, UK',
                      'destination_lat': 57.1497,
                      'destination_lng': -2.0943,
                      'date':
                          '${DateFormat('dd/MM/yy, HH:mm').format(DateTime.now().add(Duration(hours: 3)))}',
                      'first_name': 'Isaac',
                      'last_name': 'Asimov',
                      'email': 'isaac.asimov@gmail.com',
                      'phone': '+447891234561',
                    });
                    setLoading(false);
                  },
                  child: Text('Open Karhoo Screen'),
                ),
        ),
      ),
    );
  }

  void setLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }
}
