//
//  KarhooConfig.swift
//  Runner
//
//  Created by Eugene Kurkel on 09.07.2021.
//

import KarhooUISDK
import KarhooSDK

struct KarhooConfig: KarhooUISDKConfiguration {

    func environment() -> KarhooEnvironment {
        return .sandbox
    }

    func authenticationMethod() -> AuthenticationMethod {
        return .guest(settings: GuestSettings(identifier:"FQnITy6NWgtj00HdEC5EflKHvqa0kSVq",
                                              referer: "https://mobile.karhoo.com",
                                              organisationId: "7f561990-8141-4192-bbf4-538fddaa3179"))
    }
}
